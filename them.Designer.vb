﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class them
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim MANVLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(them))
        Dim HOLOTLabel As System.Windows.Forms.Label
        Me.QLNSDataSet = New quanlinhansu.QLNSDataSet()
        Me.NHANVIENBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NHANVIENTableAdapter = New quanlinhansu.QLNSDataSetTableAdapters.NHANVIENTableAdapter()
        Me.TableAdapterManager = New quanlinhansu.QLNSDataSetTableAdapters.TableAdapterManager()
        Me.NHANVIENBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.MANVTextBox = New System.Windows.Forms.TextBox()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.NHANVIENBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.HOLOTTextBox = New System.Windows.Forms.TextBox()
        MANVLabel = New System.Windows.Forms.Label()
        HOLOTLabel = New System.Windows.Forms.Label()
        CType(Me.QLNSDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NHANVIENBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NHANVIENBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NHANVIENBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'QLNSDataSet
        '
        Me.QLNSDataSet.DataSetName = "QLNSDataSet"
        Me.QLNSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NHANVIENBindingSource
        '
        Me.NHANVIENBindingSource.DataMember = "NHANVIEN"
        Me.NHANVIENBindingSource.DataSource = Me.QLNSDataSet
        '
        'NHANVIENTableAdapter
        '
        Me.NHANVIENTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CHUCVUTableAdapter = Nothing
        Me.TableAdapterManager.LUONGTableAdapter = Nothing
        Me.TableAdapterManager.NHANVIENTableAdapter = Me.NHANVIENTableAdapter
        Me.TableAdapterManager.PHONGTableAdapter = Nothing
        Me.TableAdapterManager.taikhoanTableAdapter = Nothing
        Me.TableAdapterManager.TRINHDOTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = quanlinhansu.QLNSDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'NHANVIENBindingNavigator
        '
        Me.NHANVIENBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.NHANVIENBindingNavigator.BindingSource = Me.NHANVIENBindingSource
        Me.NHANVIENBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.NHANVIENBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.NHANVIENBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.NHANVIENBindingNavigatorSaveItem})
        Me.NHANVIENBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.NHANVIENBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.NHANVIENBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.NHANVIENBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.NHANVIENBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.NHANVIENBindingNavigator.Name = "NHANVIENBindingNavigator"
        Me.NHANVIENBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.NHANVIENBindingNavigator.Size = New System.Drawing.Size(783, 25)
        Me.NHANVIENBindingNavigator.TabIndex = 0
        Me.NHANVIENBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'MANVLabel
        '
        MANVLabel.AutoSize = True
        MANVLabel.Location = New System.Drawing.Point(61, 49)
        MANVLabel.Name = "MANVLabel"
        MANVLabel.Size = New System.Drawing.Size(41, 13)
        MANVLabel.TabIndex = 1
        MANVLabel.Text = "MANV:"
        '
        'MANVTextBox
        '
        Me.MANVTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.NHANVIENBindingSource, "MANV", True))
        Me.MANVTextBox.Location = New System.Drawing.Point(108, 46)
        Me.MANVTextBox.Name = "MANVTextBox"
        Me.MANVTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MANVTextBox.TabIndex = 2
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'NHANVIENBindingNavigatorSaveItem
        '
        Me.NHANVIENBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.NHANVIENBindingNavigatorSaveItem.Image = CType(resources.GetObject("NHANVIENBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.NHANVIENBindingNavigatorSaveItem.Name = "NHANVIENBindingNavigatorSaveItem"
        Me.NHANVIENBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.NHANVIENBindingNavigatorSaveItem.Text = "Save Data"
        '
        'HOLOTLabel
        '
        HOLOTLabel.AutoSize = True
        HOLOTLabel.Location = New System.Drawing.Point(265, 58)
        HOLOTLabel.Name = "HOLOTLabel"
        HOLOTLabel.Size = New System.Drawing.Size(47, 13)
        HOLOTLabel.TabIndex = 3
        HOLOTLabel.Text = "HOLOT:"
        '
        'HOLOTTextBox
        '
        Me.HOLOTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.NHANVIENBindingSource, "HOLOT", True))
        Me.HOLOTTextBox.Location = New System.Drawing.Point(318, 55)
        Me.HOLOTTextBox.Name = "HOLOTTextBox"
        Me.HOLOTTextBox.Size = New System.Drawing.Size(100, 20)
        Me.HOLOTTextBox.TabIndex = 4
        '
        'them
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(783, 429)
        Me.Controls.Add(HOLOTLabel)
        Me.Controls.Add(Me.HOLOTTextBox)
        Me.Controls.Add(MANVLabel)
        Me.Controls.Add(Me.MANVTextBox)
        Me.Controls.Add(Me.NHANVIENBindingNavigator)
        Me.Name = "them"
        Me.Text = "them"
        CType(Me.QLNSDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NHANVIENBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NHANVIENBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NHANVIENBindingNavigator.ResumeLayout(False)
        Me.NHANVIENBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents QLNSDataSet As quanlinhansu.QLNSDataSet
    Friend WithEvents NHANVIENBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NHANVIENTableAdapter As quanlinhansu.QLNSDataSetTableAdapters.NHANVIENTableAdapter
    Friend WithEvents TableAdapterManager As quanlinhansu.QLNSDataSetTableAdapters.TableAdapterManager
    Friend WithEvents NHANVIENBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents NHANVIENBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents MANVTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HOLOTTextBox As System.Windows.Forms.TextBox
End Class
