﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(login))
        Me.UserTextBox = New System.Windows.Forms.TextBox()
        Me.TaikhoanBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.QLNSDataSet = New quanlinhansu.QLNSDataSet()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TaikhoanTableAdapter = New quanlinhansu.QLNSDataSetTableAdapters.taikhoanTableAdapter()
        Me.TableAdapterManager = New quanlinhansu.QLNSDataSetTableAdapters.TableAdapterManager()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        CType(Me.TaikhoanBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QLNSDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UserTextBox
        '
        Me.UserTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TaikhoanBindingSource, "User", True))
        Me.UserTextBox.Location = New System.Drawing.Point(133, 124)
        Me.UserTextBox.Name = "UserTextBox"
        Me.UserTextBox.Size = New System.Drawing.Size(142, 20)
        Me.UserTextBox.TabIndex = 2
        Me.UserTextBox.Tag = "ấ"
        '
        'TaikhoanBindingSource
        '
        Me.TaikhoanBindingSource.DataMember = "taikhoan"
        Me.TaikhoanBindingSource.DataSource = Me.QLNSDataSet
        '
        'QLNSDataSet
        '
        Me.QLNSDataSet.DataSetName = "QLNSDataSet"
        Me.QLNSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TaikhoanBindingSource, "password", True))
        Me.PasswordTextBox.Location = New System.Drawing.Point(133, 163)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasswordTextBox.Size = New System.Drawing.Size(142, 20)
        Me.PasswordTextBox.TabIndex = 4
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.Font = New System.Drawing.Font("VNI-DOS Sample Font ", 9.749999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(145, 200)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(117, 40)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Đăng nhập"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TaikhoanTableAdapter
        '
        Me.TaikhoanTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CHUCVUTableAdapter = Nothing
        Me.TableAdapterManager.LUONGTableAdapter = Nothing
        Me.TableAdapterManager.NHANVIENTableAdapter = Nothing
        Me.TableAdapterManager.PHONGTableAdapter = Nothing
        Me.TableAdapterManager.taikhoanTableAdapter = Me.TaikhoanTableAdapter
        Me.TableAdapterManager.TRINHDOTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = quanlinhansu.QLNSDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Blue
        Me.Label1.ForeColor = System.Drawing.Color.Coral
        Me.Label1.Location = New System.Drawing.Point(129, 108)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Tag = "a"
        Me.Label1.Text = "Tên Người Dùng"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Blue
        Me.Label2.ForeColor = System.Drawing.Color.Coral
        Me.Label2.Location = New System.Drawing.Point(129, 147)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Tag = "a"
        Me.Label2.Text = "Mật Khẩu"
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Navy
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Coral
        Me.Label3.Location = New System.Drawing.Point(114, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(197, 24)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "QUẢN LÝ NHÂN SỰ"
        '
        'login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = Global.quanlinhansu.My.Resources.Resources.chienluocnhansu
        Me.ClientSize = New System.Drawing.Size(410, 261)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PasswordTextBox)
        Me.Controls.Add(Me.UserTextBox)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "login"
        Me.Text = "ĐĂNG NHẬP TÀI KHOẢN"
        CType(Me.TaikhoanBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QLNSDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents QLNSDataSet As quanlinhansu.QLNSDataSet
    Friend WithEvents TaikhoanBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TaikhoanTableAdapter As quanlinhansu.QLNSDataSetTableAdapters.taikhoanTableAdapter
    Friend WithEvents TableAdapterManager As quanlinhansu.QLNSDataSetTableAdapters.TableAdapterManager
    Friend WithEvents UserTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label3 As System.Windows.Forms.Label

End Class
